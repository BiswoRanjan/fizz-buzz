﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzzTests.cs" company="TCS">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Fizz_Buzz.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using NUnit.Framework;

    /// <summary>
    /// Class for Test fizz and buzz.
    /// </summary>
    [TestFixture]
    public class FizzBuzzTests
    {
        /// <summary>
        /// Variable for buzz.
        /// </summary>
        private TestBuzz testBuzz;

        /// <summary>
        /// Variable for fizz.
        /// </summary>
        private TestFizz testFizz;

        /// <summary>
        /// Variable for fizz and buzz.
        /// </summary>
        private TestFizzBuzz testFizzBuzz;

        /// <summary>
        /// Variable for page default.
        /// </summary>
        private TestDefault testDefault;

        /// <summary>
        /// Initializes instances of classes/> class.
        /// </summary>       
        [SetUp]
        public void Setup()
        {
            this.testBuzz = new TestBuzz();
            this.testFizz = new TestFizz();
            this.testFizzBuzz = new TestFizzBuzz();
            this.testDefault = new TestDefault();
        }

        /// <summary>
        /// Test method to check whether no is divisible by 5 or not.
        /// </summary>
        [Test]
        public void Display_Divisibleby5NotonWedensday_shouldreturnbuzz()
        {
            var result = this.testBuzz.Display_Divisibleby5NotonWedensday_ShouldreturnBuzz(5, DayOfWeek.Monday);

            Assert.AreEqual("buzz", result);
        }

        /// <summary>
        /// Test method to check whether no is divisible by 5 or not.
        /// </summary>
        [Test]
        public void Display_Divisibleby5onWedensday_shouldreturnwuzz()
        {
            var result = this.testBuzz.Display_Divisibleby5onWedensday_ShouldreturnWuzz(5, DayOfWeek.Wednesday);
            Assert.AreEqual("wuzz", result);
        }

        /// <summary>
        /// Test method to check whether no is divisible by 3 or not.
        /// </summary>
        [Test]
        public void Display_Divisibleby3NotonWedensday_shouldreturnfizz()
        {
            var result = this.testFizz.Display_Divisibleby3NotonWedensday_ShouldreturnFizz(3, DayOfWeek.Monday);
            Assert.AreEqual("fizz", result);
        }

        /// <summary>
        /// Test method to check whether no is divisible by 3 or not.
        /// </summary>
        [Test]
        public void Display_Divisibleby3onWedensday_shouldreturnwizz()
        {
            var result = this.testFizz.Display_Divisibleby3NotonWedensday_ShouldreturnFizz(3, DayOfWeek.Wednesday);
            Assert.AreEqual("wizz", result);
        }

        /// <summary>
        /// Test method to check whether no is divisible by 15 or not.
        /// </summary>
        [Test]
        public void Display_Divisibleby5And3NotonWedensday_shouldreturnfizzbuzz()
        {
            var result = this.testFizzBuzz.Display_Divisibleby5And3NotonWedensday_Shouldreturnfizzbuzz(15, DayOfWeek.Monday);
            Assert.AreEqual("fizz buzz", result);
        }

        /// <summary>
        /// Test method to check whether no is divisible by 15 or not.
        /// </summary>s
        [Test]
        public void Display_Divisibleby5And3onWedensday_shouldreturnwizzwuzz()
        {
            var result = this.testFizzBuzz.Display_Divisibleby5And3onWedensday_Shouldreturnwizzwuzz(15, DayOfWeek.Wednesday);
            Assert.AreEqual("wizz wuzz", result);
        }

        /// <summary>
        /// Test method to check default no.. 
        /// </summary>
        [Test]
        public void Display_Default_ShouldReturnOriginalNo()
        {
            var result = this.testDefault.Display_Default_ShouldReturnOriginalNo(11, DayOfWeek.Wednesday);
            Assert.AreEqual("11", result);
        }
    }
}
