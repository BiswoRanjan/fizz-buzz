﻿//-----------------------------------------------------------------------
// <copyright file="TestFizzBuzz.cs" company="TCS">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Fizz_Buzz.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class for Test fizz and buzz.
    /// </summary>
    public class TestFizzBuzz
    {
        /// <summary>
        /// Method returns the output based on input. 
        /// </summary>
        /// <param name="input">first parameter</param>
        /// <param name="day">second parameter</param>
        /// <returns>Return string value.</returns>
        public string Display_Divisibleby5And3NotonWedensday_Shouldreturnfizzbuzz(int input, DayOfWeek day)
        {
            return input % 15 == 0 && DayOfWeek.Wednesday != day ? "fizz buzz" : "wizz wuzz";
        }

        /// <summary>
        /// Method returns the output based on input. 
        /// </summary>
        /// <param name="input">first parameter</param>
        /// <param name="day">second parameter</param>
        /// <returns>Return string value.</returns>
        public string Display_Divisibleby5And3onWedensday_Shouldreturnwizzwuzz(int input, DayOfWeek day)
        {
            return input % 15 == 0 && DayOfWeek.Wednesday == day ? "wizz wuzz" : "fizz buzz";
        }
    }
}
