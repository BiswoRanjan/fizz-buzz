﻿//-----------------------------------------------------------------------
// <copyright file="TestBuzz.cs" company="TCS">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Fizz_Buzz.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class for Test buzz.
    /// </summary>
   public class TestBuzz
    {
        /// <summary>
        /// Method returns the output based on input. 
        /// </summary>
        /// <param name="input">first parameter</param>
        /// <param name="day">second parameter</param>
        /// <returns>Return string value.</returns>
        public string Display_Divisibleby5NotonWedensday_ShouldreturnBuzz(int input, DayOfWeek day)
        {
            return input % 5 == 0 && DayOfWeek.Wednesday != day ? "buzz" : "wuzz";
        }

        /// <summary>
        /// Print method returns the output based on input. 
        /// </summary>
        /// <param name="input">first parameter</param>
        /// <param name="day">second parameter</param>
        /// <returns>Return string value.</returns>
        public string Display_Divisibleby5onWedensday_ShouldreturnWuzz(int input, DayOfWeek day)
        {
            return input % 5 == 0 && DayOfWeek.Wednesday == day ? "wuzz" : "buzz";
        }
    }
}
