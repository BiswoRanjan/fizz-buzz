﻿//-----------------------------------------------------------------------
// <copyright file="TestFizz.cs" company="TCS">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Fizz_Buzz.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class for Test fizz.
    /// </summary>
    public class TestFizz
    {
        /// <summary>
        /// Method returns the output based on input. 
        /// </summary>
        /// <param name="input">first parameter</param>
        /// <param name="day">second parameter</param>
        /// <returns>Return string value.</returns>
        public string Display_Divisibleby3NotonWedensday_ShouldreturnFizz(int input, DayOfWeek day)
        {
            return input % 3 == 0 && DayOfWeek.Wednesday != day ? "fizz" : "wizz";
        }

        /// <summary>
        /// Method returns the output based on input. 
        /// </summary>
        /// <param name="input">first parameter</param>
        /// <param name="day">second parameter</param>
        /// <returns>Return string value.</returns>
        public string Display_Divisibleby3onWedensday_ShouldreturnWizz(int input, DayOfWeek day)
        {
            return input % 3 == 0 && DayOfWeek.Wednesday == day ? "wizz" : "fizz";
        }
    }
}
