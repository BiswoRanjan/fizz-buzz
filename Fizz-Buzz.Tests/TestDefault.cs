﻿//-----------------------------------------------------------------------
// <copyright file="TestDefault.cs" company="TCS">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Fizz_Buzz.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class for Test Default.
    /// </summary>
    public class TestDefault
    {
        /// <summary>
        /// Method returns the output based on input. 
        /// </summary>
        /// <param name="input">first parameter</param>
        /// <param name="day">second parameter</param>
        /// <returns>Return string value.</returns>
        public string Display_Default_ShouldReturnOriginalNo(int input, DayOfWeek day)
        {
            return input % 5 != 0 && input % 3 != 0 ? input.ToString() : string.Empty;
        }
    }
}
