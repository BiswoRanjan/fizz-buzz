﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzzModel.cs" company="TCS">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Fizz_Buzz.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web;
    using PagedList;

    /// <summary>
    /// Model class for fizz and buzz test.
    /// </summary>
    public class FizzBuzzModel
    {
        /// <summary>
        /// Gets or sets the page.
        /// </summary>
        public int? Page { get; set; }

        /// <summary>
        /// Gets or sets the Number.
        /// </summary>
        [Required(ErrorMessage = "Number field can not be empty")]
        [Range(1, 1000, ErrorMessage = "Please enter value in between 1 to 1000")]
        [RegularExpression(@"^[1-9]\d*$", ErrorMessage = "Please enter postive number")]
        public int Number { get; set; }

        /// <summary>
        /// Gets or sets the SearchResults.
        /// </summary>
        public IPagedList<string> SearchResults { get; set; }
    }
}