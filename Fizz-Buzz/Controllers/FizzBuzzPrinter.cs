﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzzPrinter.cs" company="TCS">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Fizz_Buzz.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    /// <summary>
    /// Class for fizz and buzz printer.
    /// </summary>
    public class FizzBuzzPrinter : IPrint
    {
        /// <summary>
        /// Variable for next object injection..
        /// </summary>
        private IPrint nextFB = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzPrinter"/> class.
        /// </summary>       
        /// <param name="next">first parameter</param>
        public FizzBuzzPrinter(IPrint next)
        {
            this.nextFB = next;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzPrinter"/> class.
        /// </summary> 
        public FizzBuzzPrinter()
        {
        }

        /// <summary>
        /// Print method returns the output based on input. 
        /// </summary>
        /// <param name="number">first parameter</param>
        /// <param name="day">second parameter</param>
        /// <returns>Return string value.</returns>
        public string Print(int number, DayOfWeek day)
        {
            return number % 15 == 0 && DayOfWeek.Wednesday == day ? "wizz wuzz" : number % 15 == 0 ? "fizz buzz" : this.nextFB != null ? this.nextFB.Print(number, day) : string.Empty;
        }
    }
}