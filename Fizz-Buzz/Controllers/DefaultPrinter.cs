﻿//-----------------------------------------------------------------------
// <copyright file="DefaultPrinter.cs" company="TCS">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Fizz_Buzz.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    /// <summary>
    /// Class for default printer.
    /// </summary>
    public class DefaultPrinter : IPrint
    {
        /// <summary>
        /// Variable for next object injection..
        /// </summary>
        private IPrint nextDefault = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultPrinter"/> class.
        /// </summary>       
        /// <param name="next">first parameter</param>
        public DefaultPrinter(IPrint next)
        {
            this.nextDefault = next;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultPrinter"/> class.
        /// </summary>  
        public DefaultPrinter() 
        {
        }

        /// <summary>
        /// Print method returns the output based on input. 
        /// </summary>
        /// <param name="number">first parameter</param>
        /// <param name="day">second parameter</param>
        /// <returns>Return string value.</returns>
        public string Print(int number, DayOfWeek day)
        {
            return number % 5 != 0 && number % 3 != 0 ? number.ToString() : this.nextDefault != null ? this.nextDefault.Print(number, day) : string.Empty;
        }
    }
}