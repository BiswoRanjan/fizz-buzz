﻿//-----------------------------------------------------------------------
// <copyright file="Ioc.cs" company="TCS">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Fizz_Buzz.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Xml;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using Castle.Windsor.Configuration.Interpreters;

    /// <summary>
    /// Class for Inversion of control.
    /// </summary>
    public static class Ioc
    {
        /// <summary>
        /// Variable for container object.
        /// </summary>
        private static IWindsorContainer container = null;

        /// <summary>
        /// method to resolve dependency. 
        /// </summary>
        /// <typeparam  name="T">first parameter</typeparam >
        /// <returns>Return T</returns>
        public static T Resolve<T>() where T : class
        {
            InitializeWindsorContainer();
            return container.Resolve<T>();
        }

        /// <summary>
        /// Initialize classes from configuration file.
        /// </summary>
        private static void InitializeWindsorContainer()
        {
            try
            {
                if (container == null)
                {
                    StringBuilder basePath = new StringBuilder(AppDomain.CurrentDomain.BaseDirectory);
                    basePath.Append(@"IocConfiguration.xml");
                    container = new WindsorContainer(new XmlInterpreter(basePath.ToString()));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}