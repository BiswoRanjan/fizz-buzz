﻿//-----------------------------------------------------------------------
// <copyright file="Buzzprinter.cs" company="TCS">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Fizz_Buzz.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    /// <summary>
    /// Class for buzz printer.
    /// </summary>
    public class Buzzprinter : IPrint
    {
        /// <summary>
        /// Variable for next object injection..
        /// </summary>
        private IPrint nextBuzz = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="Buzzprinter"/> class.
        /// </summary>       
        /// <param name="next">first parameter</param>
        public Buzzprinter(IPrint next)
        {
            this.nextBuzz = next;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Buzzprinter"/> class.
        /// </summary>       
        public Buzzprinter()
        {
        }

        /// <summary>
        /// Print method returns the output based on input. 
        /// </summary>
        /// <param name="number">first parameter</param>
        /// <param name="day">second parameter</param>
        /// <returns>Return string value.</returns>
        public string Print(int number, DayOfWeek day)
        {
            return number % 5 == 0 && DayOfWeek.Wednesday == day ? "wuzz" : number % 5 == 0 ? "buzz" : this.nextBuzz != null ? this.nextBuzz.Print(number, day) : string.Empty;
        }
    }
}