﻿//-----------------------------------------------------------------------
// <copyright file="Fizzprinter.cs" company="TCS">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Fizz_Buzz.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    /// <summary>
    /// Class for fizz printer.
    /// </summary>
    public class Fizzprinter : IPrint
    {
        /// <summary>
        /// Variable for next object injection..
        /// </summary>
        private IPrint nextP = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="Fizzprinter"/> class.
        /// </summary>       
        /// <param name="next">first parameter</param>
        public Fizzprinter(IPrint next)
        {
            this.nextP = next;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Fizzprinter"/> class.
        /// </summary>
        public Fizzprinter()
        {
        }

        /// <summary>
        /// Print method returns the output based on input. 
        /// </summary>
        /// <param name="number">first parameter</param>
        /// <param name="day">second parameter</param>
        /// <returns>Return string value.</returns>
        public string Print(int number, DayOfWeek day)
        {
            return number % 3 == 0 && DayOfWeek.Wednesday == day ? "wizz" : number % 3 == 0 ? "fizz" : this.nextP != null ? this.nextP.Print(number, day) : string.Empty;
        }
    }
}