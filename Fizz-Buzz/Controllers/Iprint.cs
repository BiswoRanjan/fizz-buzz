﻿//-----------------------------------------------------------------------
// <copyright file="IPrint.cs" company="TCS">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Fizz_Buzz.Controllers
{
    using System;

    /// <summary> 
    /// Interface Print.
    /// </summary>
    public interface IPrint
    {
        /// <summary>
        /// Print method returns the output based on input. 
        /// </summary>
        /// <param name="number">first parameter</param>
        /// <param name="day">second parameter</param>
        /// <returns>Return string value.</returns>
        string Print(int number, DayOfWeek day);
    }
}
