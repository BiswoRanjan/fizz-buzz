﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzzcontroller.cs" company="TCS">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Fizz_Buzz.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Fizz_Buzz.Models;
    using PagedList;

    /// <summary>
    /// fizz and buzz controller class
    /// </summary>
    public class FizzBuzzcontroller : Controller
    {
        /// <summary>
        /// Variable for page size.
        /// </summary>
        private const int ConstRecordsPerpage = 20;

        /// <summary>
        /// fizz and buzz action method. 
        /// </summary>
        /// <param name="objModel">first parameter</param>
        /// <returns>Return ActionResult</returns>
        public ActionResult FizzBuzz(FizzBuzzModel objModel)
        {
            List<string> displayItems = new List<string>();
            if (ModelState.IsValid)
            {
                IPrint printController = Ioc.Resolve<IPrint>();
                string value = string.Empty;
                for (int count = 1; count <= objModel.Number; count++)
                {
                    value = printController.Print(count, DateTime.Today.DayOfWeek);
                    if (value == "fizz" || value == "wizz")
                    {
                        value = "<li style='list-style: none; color: blue;'>" + value + "</li>";
                    }
                    else
                    {
                        if (value == "buzz" || value == "wuzz")
                        {
                            value = "<li style='list-style: none; color: green;'>" + value + "</li>";
                        }
                        else
                        {
                            value = "<li style='list-style: none;'>" + value + "</li>";
                        }
                    }

                    displayItems.Add(value);
                }

                var pageIndex = objModel.Page ?? 1;
                objModel.SearchResults = displayItems.ToPagedList(pageIndex, ConstRecordsPerpage);
            }

            return this.View(objModel);
        }
    }
}
